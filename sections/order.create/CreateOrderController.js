
(function (){
    'use strict';
    angular.module('app').controller('CreateOrderController',
        ['$scope', '$state','$stateParams','Authentication',"OrderService","CategoryService",'ItemService','WorkpointService','$log','MessageService','NavigationService',
            function($scope,$state,$stateParams, Authentication,OrderService,CategoryService,ItemService,WorkpointService,$log,MessageService,NavigationService) {

                NavigationService.activeOrder();


                $scope.order = OrderService.get($stateParams.id);


                $scope.categories = CategoryService.getAll();

                $scope.items = ItemService.getAll();

                $scope.workpoints = WorkpointService.getAll();

                $scope.workpoint = WorkpointService.get($scope.currentUser.workpointId);


                $scope.navCreate = true;
                $scope.animateClass = "users";


                $scope.orderItems = OrderService.getOrderItems($scope.order);

                $scope.removeOrderItem = function(orderItem) {
                    $scope.orderItems.$remove(orderItem).then(function() {
                        MessageService.setSuccess("Удаленна позиция из заказа!");
                    });
                };

                $scope.displayForm = function () {
                    return $scope.categories.length > 0 && $scope.workpoints.length> 0 && $scope.items.length > 0;
                };

                $scope.calculateCost  = function () {
                    return OrderService.calculateTotalCost($scope.orderItems,$scope.order.customCost,$scope.order.discount,
                    $scope.workpoint.percentRate
                    );
                };

                $scope.saveTemp = function() {
                    OrderService.temp($scope.order,$scope.orderItems,$scope.workpoint);
                };

                $scope.finish = function () {
                    OrderService.finish($scope.order,$scope.orderItems,$scope.workpoint)
                };

            }]);
} // Controller

());