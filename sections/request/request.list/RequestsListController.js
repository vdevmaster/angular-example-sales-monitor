
(function (){
    'use strict';
    angular.module('app').controller('RequestsListController',
        ['$scope', 'Authentication',"RequestService",'NavigationService','WorkpointService',
        function($scope, Authentication,RequestService,NavigationService,WorkpointService) {

            NavigationService.activeRequest();
            $scope.navList = true;

            $scope.requests = RequestService.getAll();

            var setNumbers = function() {

                var numbers = _.filter($scope.requests, function (request) {

                    if(request.userCreated === $scope.currentUser.$id) {
                        return true;
                    }else if(request.$id != $scope.currentUser.$id && $scope.currentUser.isOwner) {
                        return true;
                    }else {
                        return false;
                    }

                });

                if(numbers) {
                    numbers = numbers.length;
                }else{
                    numbers = 0;
                }

                $scope.numbersRequest = numbers;
            };

            $scope.$watch('requests.length', function () {
                setNumbers();
            });

            $scope.startDate = moment().subtract(30,'d').format('DD.MM.YYYY');
            $scope.endDate = moment().add(1,"d").format('DD.MM.YYYY');

            $scope.getWorkpointName= function(workpointId) {
                return WorkpointService.getName(workpointId);
            };

        }]);


} // Controller

());