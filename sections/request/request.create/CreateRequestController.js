
(function (){
    'use strict';
    angular.module('app').controller('CreateRequestController',
        ['$scope', 'Authentication',"RequestService",'NavigationService','WorkpointService',
            function($scope, Authentication,RequestService,NavigationService,WorkpointService) {
                NavigationService.activeRequest();
                $scope.navCreate = true;
                $scope.request = {};

                var workpoint = WorkpointService.get($scope.currentUser.workpointId);
                $scope.create =function() {
                    RequestService.create(angular.copy($scope.request),workpoint);
                };
            }]);
} // Controller

());