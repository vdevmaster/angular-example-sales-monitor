
(function (){
    'use strict';
    angular.module('app').controller('WorkpointsListController',
        ['$scope', 'Authentication',"WorkpointService",'NavigationService',
        function($scope, Authentication,WorkpointService,NavigationService) {

            NavigationService.activeWorkpoint();
            $scope.navList = true;


            $scope.workpoints = WorkpointService.getAll();


            $scope.$watch('workpoints', function () {
                if ($scope.workpoints) {
                    $scope.numbersWorkpoint = $scope.workpoints.length
                }
            });

        }]);


} // Controller

());