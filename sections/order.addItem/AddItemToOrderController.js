
(function (){
    'use strict';
    angular.module('app').controller('AddItemToOrderController',
        ['$scope', '$state','$stateParams','Authentication',"OrderService","CategoryService",'ItemService','WorkpointService','$log','NavigationService',
            function($scope,$state,$stateParams, Authentication,OrderService,CategoryService,ItemService,WorkpointService,$log,NavigationService) {

                NavigationService.activeOrder();

                $scope.order = OrderService.get($stateParams.id);

                $scope.categories = CategoryService.getAll();

                $scope.items = ItemService.getAll();


                $scope.animateClass = "users";

                $scope.orderItem = {};
                $scope.orderItem.count = 1;

                $scope.create = function () {

                    OrderService.createOderItem($scope.order, $scope.orderItem);
                };

                $scope.calculateCost = function() {

                    if(angular.isDefined($scope.orderItem.item) && angular.isDefined($scope.orderItem.item.price)) {
                        return OrderService.calculatePriceForOderItem($scope.orderItem);
                    }else {
                        return 0;
                    }
                };

            }]);
} // Controller

());