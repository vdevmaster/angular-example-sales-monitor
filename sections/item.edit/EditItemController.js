
(function (){
    'use strict';
    angular.module('app').controller('EditItemController',
        ['$scope', 'Authentication','$state','$stateParams','ItemService','$log','CategoryService','NavigationService',
            function($scope, Authentication,$state,$stateParams,ItemService,$log,CategoryService,NavigationService) {
                NavigationService.activeItem();
                var editId = $stateParams.id;

                $scope.item = ItemService.get(editId);

                $scope.categories = CategoryService.getAll();

                $scope.item.category = CategoryService.get($scope.item.categoryId);

                $scope.animateClass = "users";

                $scope.navEdit = true;



                $scope.update = function () {
                    ItemService.update($scope.item);

                }

            }]);
} // Controller

());