(function (){
    'use strict';
    angular.module('app').controller('CategoryController','NavigationService',
        ['$scope', 'Authentication',
            function($scope, Authentication,NavigationService) {
                NavigationService.activeCategory();
            }]);
} // Controller

());