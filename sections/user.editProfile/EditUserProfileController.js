
(function (){
    'use strict';
    angular.module('app').controller('EditUserProfileController',
        ['$scope', 'Authentication','$state','$stateParams','UserService','$log','WorkpointService','NavigationService',
            function($scope, Authentication,$state,$stateParams,UserService,$log,WorkpointService,NavigationService) {

                NavigationService.activeUser();
                $scope.user = $scope.currentUser;
                $scope.workpoints = WorkpointService.getAll();

                if($scope.user.workpointId) {
                    $scope.user.workpoint = WorkpointService.get($scope.user.workpointId);
                }else{

                }
                $scope.animateClass = "users";

                $scope.navEdit = true;


                $scope.update = function () {
                    UserService.updateProfile($scope.user);
                }

            }]);
} // Controller

());