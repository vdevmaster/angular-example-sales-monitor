(function (){
    'use strict';
    angular.module('app').controller('RegistrationController',
        ['$scope', 'Authentication','NavigationService',
            function($scope, Authentication,NavigationService) {
                NavigationService.activeReg();
                $scope.register = function() {
                    Authentication.register($scope.user);

                };
            }]);
} // Controller

());

