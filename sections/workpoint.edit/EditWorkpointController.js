(function () {
    'use strict';
    angular.module('app').controller('EditWorkpointController',
        ['$scope', 'Authentication', 'WorkpointService', '$stateParams', 'NavigationService',
            function ($scope, Authentication, WorkpointService, $stateParams, NavigationService) {
                var id = $stateParams.id;
                $scope.workpoint = WorkpointService.get(id);
                NavigationService.activeWorkpoint();
                $scope.navEdit = true;

                $scope.update = function () {
                    WorkpointService.update($scope.workpoint);
                };


            }]);
} // Controller

());