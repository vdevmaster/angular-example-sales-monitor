
(function (){
    'use strict';
    angular.module('app').controller('CreateUserController',
        ['$scope', '$state','Authentication',"UserService","WorkpointService",'NavigationService',
            function($scope,$state, Authentication,UserService,WorkpointService,NavigationService) {
                NavigationService.activeUser();
                $scope.user = {};

                $scope.workpoints = WorkpointService.getAll();

                $scope.navCreate = true;

                $scope.animateClass = "users";

                $scope.create = function() {
                    $scope.user.enabled = true;
                    UserService.create($scope.user);
                };

            }]);
} // Controller

());