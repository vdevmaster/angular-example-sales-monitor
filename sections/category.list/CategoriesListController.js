
(function (){
    'use strict';
    angular.module('app').controller('CategoriesListController',
        ['$scope', 'Authentication',"CategoryService","$stateParams","MessageService",'NavigationService',
        function($scope, Authentication,CategoryService,$stateParams,MessageService,NavigationService) {
            NavigationService.activeCategory();
            $scope.navList = true;

            $scope.categories = CategoryService.getAll();

            var updateNumbers = function() {
                $scope.numbersCategory = $scope.categories.length;
            };

            updateNumbers();

            $scope.$watch('categories', function () {
                updateNumbers();
            });



        }]);


} // Controller

());