
(function (){
    'use strict';
    angular.module('app').controller('OrderController',
        ['$scope', 'Authentication','NavigationService',
            function($rootScope,$scope, Authentication,NavigationService) {
                NavigationService.activeOrder();
            }]);
} // Controller

());