
(function (){
    'use strict';
    angular.module('app').controller('ItemsListController',
        ['$scope','$log', 'Authentication','ItemService','CategoryService','NavigationService',
            function($scope,$log ,Authentication,ItemService,CategoryService,NavigationService) {

                NavigationService.activeItem();

                $scope.items = ItemService.getAll();


                $scope.$watch('items', function () {
                    if ($scope.items) {
                        $scope.numbersItem = $scope.items.length;
                    }
                });


                $scope.navList = true;
                $scope.categories = CategoryService.getAll();
                $scope.animateClass = "users";


                var findCategoryForItem = function(item) {
                    return _.filter($scope.categories, function (cat) {
                        return cat.$id == item.categoryId;
                    });
                };

                $scope.getCategoryName = function (item) {
                    return findCategoryForItem(item)[0].title;
                };


            }]);
} // Controller

());