
(function (){
    'use strict';
    angular.module('app').controller('ItemController',
        ['$scope', 'Authentication','NavigationService',
            function($scope, Authentication,NavigationService) {
                NavigationService.activeItem();
            }]);
} // Controller

());