
(function (){
    'use strict';
    angular.module('app').controller('EditCategoryController',
        ['$scope', 'Authentication','CategoryService','$stateParams','NavigationService',
            function($scope, Authentication,CategoryService,$stateParams,NavigationService) {
                NavigationService.activeCategory();
                var id = $stateParams.id;
                $scope.category = CategoryService.get(id);

                $scope.navEdit = true;

$scope.update = function() {
    CategoryService.update($scope.category);
};


            }]);
} // Controller

());