(function (){
  'use strict';
  angular.module('app').controller('LoginController',
      ['$scope', 'Authentication','NavigationService',
        function($scope, Authentication,NavigationService) {

            NavigationService.activeLogin();

          $scope.login = function() {

            Authentication.login($scope.user);
          }; //login

          $scope.logout = function() {
            Authentication.logout();
          }; //logout

        }]);
} // Controller

());


