
(function () {
    'use strict';
    angular.module('app').controller('NavigationController',
        ['$scope', '$log', 'Authentication',
            function ($scope, $log, Authentication) {
                $scope.logout = function () {
                    Authentication.logout();
                };
            }]);
} // Controller

());

