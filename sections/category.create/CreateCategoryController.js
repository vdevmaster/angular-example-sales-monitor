(function (){
    'use strict';
    angular.module('app').controller('CreateCategoryController',
        ['$scope', 'Authentication',"CategoryService",'NavigationService',
            function($scope, Authentication,CategoryService,NavigationService) {
                NavigationService.activeCategory();
                $scope.navCreate = true;
                $scope.category = {};
                $scope.category.priority = CategoryService.getAll().length+1;

                $scope.create =function() {
                    CategoryService.create($scope.category);
                };
            }]);
} // Controller

());