(function () {
    'use strict';
    angular.module('app').controller('OrdersListController',
        ['$scope', '$log', 'Authentication', 'OrderService','$stateParams','NavigationService','WorkpointService',
            function ($scope, $log, Authentication, OrderService,$stateParams,NavigationService,WorkpointService) {

                NavigationService.activeOrder();

                if($stateParams.create) {
                    OrderService.create();
                }


                $scope.workpoints = WorkpointService.getAll();

                var ordersList = OrderService.getAll();

                ordersList.$loaded().then(function () {
                    $scope.orders = ordersList;
                    setNumbers();
                });

                var setNumbers = function() {

                    var numbers = _.filter($scope.orders, function (order) {

                        if(order.userCreated === $scope.currentUser.$id) {
                            return true;
                        }else if(order.enabled && order.$id != $scope.currentUser.$id && $scope.currentUser.isOwner) {
                            return true;
                        }else {
                            return false;
                        }

                    });

                    if(numbers) {
                        numbers = numbers.length;
                    }else{
                        numbers = 0;
                    }

                    $scope.numbersOrder = numbers;
                };

                $scope.$watch('orders.length', function () {
                    setNumbers();
                });


                $scope.startDate = moment().subtract(30,'d').format('DD.MM.YYYY');
                $scope.endDate = moment().add(1,"d").format('DD.MM.YYYY');


                $scope.navList = true;

                $scope.animateClass = "users";

                $scope.createOrder = function() {
                    OrderService.create();
                };

                $scope.totalOrderSumm = function() {

                    if($scope.hasOwnerFeatures) {
                        return OrderService.calculateOrderCost($scope.orders);
                    }else {

                        var orders = _.filter($scope.orders,function(orderItem) {
                            return orderItem.userCreated == $scope.currentUser.$id;
                        });

                        return OrderService.calculateOrderCost(orders);

                    }

                };

                $scope.remove = function(order) {
                    OrderService.remove(order);
                };


                $scope.getWorkpointName = function(workpoint) {
                    return WorkpointService.getName(workpoint);

                };


            }]);
} // Controller

());