
(function (){
    'use strict';
    angular.module('app').controller('CreateItemController',
        ['$scope', '$state','Authentication',"ItemService","CategoryService",'NavigationService',
            function($scope,$state, Authentication,ItemService,CategoryService,NavigationService) {
                NavigationService.activeItem();
                $scope.item = {};
                $scope.categories = CategoryService.getAll();
                $scope.navCreate = true;
                $scope.animateClass = "users";

                $scope.create = function() {
                    ItemService.create($scope.item);
                };


            }]);
} // Controller

());