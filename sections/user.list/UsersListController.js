(function () {
    'use strict';
    angular.module('app').controller('UsersListController',
        ['$scope', '$log', 'Authentication', 'UserService', 'NavigationService', 'WorkpointService',
            function ($scope, $log, Authentication, UserService, NavigationService, WorkpointService) {

                NavigationService.activeUser();

                $scope.users = UserService.getAll();

                $scope.navList = true;

                $scope.animateClass = "users";

                $scope.getWorkpointName = function (workpointId) {
                    return WorkpointService.getName(workpointId);
                };


                $scope.$watch('users', function () {
                    if ($scope.users) {
                        $scope.numbersUser = $scope.users.length;
                    }
                });

            }]);
} // Controller

());