
(function (){
    'use strict';
    angular.module('app').controller('CreateWorkpointController',
        ['$scope', 'Authentication',"WorkpointService",'NavigationService',
            function($scope, Authentication,WorkpointService,NavigationService) {
                NavigationService.activeWorkpoint();
                $scope.navCreate = true;
                $scope.workpoint = {};
                $scope.workpoint.percentRate = 0;
                $scope.create =function() {
                    WorkpointService.create(angular.copy($scope.workpoint));
                };
            }]);
} // Controller

());