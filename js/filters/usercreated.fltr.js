angular.module('app').filter('usercreated', function ()
{
    return function(items,user,admin)
    {
        var result = [];

        if (items && items.length > 0)
        {
            $.each(items, function (index, item)
            {
              if (item.userCreated === user.$id){
                    result.push(item);

              }else if(admin && item.enabled) {
                  result.push(item);
              }
            });

            return result;
        }
    };
});
