angular.module('app').filter('daterange', function ()
{
    return function(items, start_date, end_date)
    {
        var result = [];
        var start;
        if(start_date.trim() === ""){
            //fix for start date
            start = moment("1980-12-01").format("YYYY-MM-DD");
        }else {
            start = moment(start_date, "DD.MM.YYYY").format("YYYY-MM-DD");
        }
        var end = moment(end_date, "DD.MM.YYYY)").format("YYYY-MM-DD");
        if (items && items.length > 0)
        {
            if(moment().isValid(start) && moment().isValid(end)) {

                $.each(items, function (index, item) {
                    var current = moment(item.created).format("YYYY-MM-DD");
                    start = new Date(start).getTime();
                    end = new Date(end).getTime();
                    current = new Date(current).getTime();
                    if (current>=start && current<=end) {
                        result.push(item);
                    }
                });
            }

            return result;
        }
    };
});
