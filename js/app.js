angular.module('app',
    ['app.dir', 'firebase', 'ui.router'
        , 'ngAnimate'
    ])
    //set url
    .constant('FIREBASE_URL', 'secret')
    .run(['$rootScope', '$state',
        function ($rootScope, $state) {
            $rootScope.$on('$stateChangeError',
                function (event, next, previous, error) {
                    if (error == 'AUTH_REQUIRED') {
                        $rootScope.message = 'Для просмотра страницы нужно зарегистрироваться';
                        $state.go('login');
                    }
                });
        }])
    .config([
        '$stateProvider', '$logProvider', '$urlRouterProvider', '$locationProvider',
        function ($stateProvider, $logProvider, $urlRouterProvider, $locationProvider) {
            $locationProvider.html5Mode({enabled: true});

            $logProvider.debugEnabled(true);

            $urlRouterProvider.otherwise('/login');

            $urlRouterProvider.when('', '/login');

            $stateProvider.state('login', {
                url: "/login",
                templateUrl: 'sections/login/login.html',
                controller: 'LoginController',
                controllerAs: "loginCtrl"
            }).state('register', {
                url: "/register",
                templateUrl: 'sections/registration/register.html',
                controller: 'RegistrationController',
                controllerAs: "regCtrl"
            }).state('user', {
                url: "/user",
                abstract: true,
                controller: "UserController",
                controllerAs: "userCtrl"
                ,
                views: {
                    '': {
                        templateUrl: "sections/user/user.html"
                    },

                    'subNavView': {
                        templateUrl: "sections/user/subnav.html"
                    }
                }

            }).state('user.list', {
                url: "/list",
                views: {
                    '': {
                        templateUrl: "sections/user.list/list.html",
                        controller: "UsersListController",
                        controllerAs: "usersListCtrl",
                    },

                    'subNavView': {
                        templateUrl: "sections/user/subnav.html",
                        controller: 'UsersListController'
                    }
                }

            }).state('user.create', {
                url: "/create",
                views: {
                    '': {
                        templateUrl: "sections/user.create/create.html",
                        controller: 'CreateUserController',
                        controllerAs: "createUserCtrl"
                    },

                    'subNavView': {
                        templateUrl: "sections/user/subnav.html",
                        controller: 'CreateUserController'
                    }
                }
            }).state('user.edit', {
                url: "/edit/:id",
                views: {
                    '': {
                        templateUrl: "sections/user.edit/edit.html",
                        controller: 'EditUserController',
                        controllerAs: "editUserCtrl"
                    },

                    'subNavView': {
                        templateUrl: "sections/user/subnav.html",
                        controller: 'EditUserController'
                    }
                },
            }).state('user.editProfile', {
                url: "/edit-profile",
                views: {
                    '': {
                        templateUrl: "sections/user.editProfile/edit_profile.html",
                        controller: 'EditUserProfileController',
                        controllerAs: "editUserCtrl"
                    },

                    'subNavView': {
                        templateUrl: "sections/user/subnav.html",
                        controller: 'EditUserProfileController'
                    }
                }
            }).state('workpoint', {
                abstract: true,
                url: "/workpoint",
                controller: "WorkpointController",
                controllerAs: "workpointCtrl"
                ,
                views: {
                    '': {
                        templateUrl: "sections/workpoint/workpoint.html"
                    },

                    'subNavView': {
                        templateUrl: "sections/workpoint/subnav.html"
                    }
                }

            }).state('workpoint.list', {
                url: "/list",
                views: {
                    '': {
                        templateUrl: "sections/workpoint.list/list.html",
                        controller: "WorkpointsListController",
                        controllerAs: "workpointsListCtrl"
                    },

                    'subNavView': {
                        templateUrl: "sections/workpoint/subnav.html"
                        , controller: 'WorkpointsListController'
                    }
                }
            }).state('workpoint.create', {
                url: "/create",
                views: {
                    '': {
                        templateUrl: "sections/workpoint.create/create.html",
                        controller: 'CreateWorkpointController',
                        controllerAs: "createWorkpointCtrl"
                    },

                    'subNavView': {
                        templateUrl: "sections/workpoint/subnav.html"
                        , controller: 'CreateWorkpointController'
                    }
                }
            }).state('workpoint.edit', {
                url: "/edit/:id",
                views: {
                    '': {
                        templateUrl: "sections/workpoint.edit/edit.html",
                        controller: 'EditWorkpointController',
                        controllerAs: "editWorkpointCtrl"
                    },

                    'subNavView': {
                        templateUrl: "sections/workpoint/subnav.html"
                        , controller: 'EditWorkpointController'
                    }
                }
            }).state('category', {
                url: "/category",
                abstract: true,
                templateUrl: 'sections/category/category.html',
                controller: 'CategoryController',
                views: {
                    '': {
                        templateUrl: "sections/category/category.html"
                    },
                    'subNavView': {
                        templateUrl: "sections/category/subnav.html"
                    }
                }

            }).state('category.list', {
                url: "/list",
                views: {
                    '': {
                        templateUrl: "sections/category.list/list.html",
                        controller: "CategoriesListController",
                        controllerAs: "categoriesListCtrl",
                    },

                    'subNavView': {
                        templateUrl: "sections/category/subnav.html",
                        controller: 'CategoriesListController'
                    }
                }
            }).state('category.create', {
                url: "/create",
                views: {
                    '': {
                        templateUrl: "sections/category.create/create.html",
                        controller: 'CreateCategoryController',
                        controllerAs: "createCategoryCtrl"
                    },

                    'subNavView': {
                        templateUrl: "sections/category/subnav.html",
                        controller: 'CreateCategoryController'
                    }
                }
            }).state('category.edit', {
                url: "/edit/:id",
                views: {
                    '': {
                        templateUrl: "sections/category.edit/edit.html",
                        controller: 'EditCategoryController',
                        controllerAs: "editCategoryCtrl"
                    },

                    'subNavView': {
                        templateUrl: "sections/category/subnav.html",
                        controller: 'EditCategoryController'
                    }
                }

            })
                .state("item", {
                    url: "/item",
                    abstract: true,
                    controller: "ItemController",
                    controllerAs: "itemCtrl"
                    ,
                    views: {
                        '': {
                            templateUrl: "sections/item/item.html"
                        },
                        'subNavView': {
                            templateUrl: "sections/item/subnav.html"
                        }
                    }
                })
                .state('item.list', {
                    url: "/list",
                    views: {
                        '': {
                            templateUrl: "sections/item.list/list.html",
                            controller: "ItemsListController",
                            controllerAs: "itemListCtrl",
                        },
                        'subNavView': {
                            templateUrl: "sections/item/subnav.html",
                            controller: 'ItemsListController'
                        }
                    }
                }).state('item.create', {
                url: "/create",
                views: {
                    '': {
                        templateUrl: "sections/item.create/create.html",
                        controller: 'CreateItemController',
                        controllerAs: "createItemCtrl"
                    },

                    'subNavView': {
                        templateUrl: "sections/item/subnav.html",
                        controller: 'CreateItemController'
                    }
                }
            }).state('item.edit', {
                url: "/edit/:id",
                views: {
                    '': {
                        templateUrl: "sections/item.edit/edit.html",
                        controller: 'EditItemController',
                        controllerAs: "editUserCtrl"
                    },

                    'subNavView': {
                        templateUrl: "sections/item/subnav.html",
                        controller: 'EditItemController'
                    }
                }
            }).state("order", {
                url: "/order",
                abstract: true,
                controller: "OrderController",
                controllerAs: "orderCtrl"
                ,
                views: {
                    '': {
                        templateUrl: "sections/order/order.html"
                    },
                    'subNavView': {
                        templateUrl: "sections/order/subnav.html"
                    }
                }
            })
                .state('order.list', {
                    url: "/list",
                    views: {
                        '': {
                            templateUrl: "sections/order.list/list.html",
                            controller: "OrdersListController",
                            controllerAs: "ordersListCtrl",
                        },

                        'subNavView': {
                            templateUrl: "sections/order/subnav.html",
                            controller: 'OrdersListController'
                        }
                    }
                }).state('order.create', {
                url: "/create/:id",
                views: {
                    '': {
                        templateUrl: "sections/order.create/create.html",
                        controller: 'CreateOrderController',
                        controllerAs: "createOrderCtrl"
                    },

                    'subNavView': {
                        templateUrl: "sections/order/subnav.html",
                        controller: 'CreateOrderController'
                    }
                }
            }).state('order.edit', {
                url: "/edit/:id",
                views: {
                    '': {
                        templateUrl: "sections/order.edit/edit.html",
                        controller: 'EditOrderController',
                        controllerAs: "editOrderCtrl"
                    },

                    'subNavView': {
                        templateUrl: "sections/order/subnav.html",
                        controller: 'EditOrderController'
                    }
                }
            }).state('order.addItem', {
                url: "/edit/:id/:itemId",
                views: {
                    '': {
                        templateUrl: "sections/order.addItem/add_item.html",
                        controller: 'AddItemToOrderController',
                        controllerAs: "addItemToOrderCtrl"
                    }
                }

            }).state("request", {
                url: "/request",
                abstract: true,
                controller: "RequestController",
                controllerAs: "reqCtrl"
                ,
                views: {
                    '': {
                        templateUrl: "sections/request/request.html"
                    },
                    'subNavView': {
                        templateUrl: "sections/request/subnav.html"
                    }
                }
            })
                .state('request.list', {
                    url: "/list",
                    views: {
                        '': {
                            templateUrl: "sections/request/request.list/list.html",
                            controller: "RequestsListController",
                            controllerAs: "reqListCtrl",
                        },

                        'subNavView': {
                            templateUrl: "sections/request/subnav.html",
                            controller: 'RequestsListController'
                        }
                    }
                }).state('request.create', {
                url: "/create",
                views: {
                    '': {
                        templateUrl: "sections/request/request.create/create.html",
                        controller: 'CreateRequestController',
                        controllerAs: "createReqCtrl"
                    },
                    'subNavView': {
                        templateUrl: "sections/request/subnav.html",
                        controller: 'CreateRequestController'
                    }
                }
            });

        }]);