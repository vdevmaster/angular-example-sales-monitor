(function () {
    'use strict';
    angular.module('app.dir').directive('btnBack',


        function () {


            var t = '<a ui-sref="{{route}}" class="btn btn-default btn-raised"><i class="material-icons">keyboard_backspace</i></a>';
            return {
                restrict: "E",

                scope: {
                    route: "@"
                },

                template: t
            };
        }
    )
    ;
} // directive

());