(function () {
    'use strict';
    angular.module('app.dir').directive('btnFabAdd',


        function () {


            var t = '<div class="pull-right"><a ui-sref="{{route}}" class="btn btn-primary btn-fab"><i class="material-icons">add</i></a></div>';
            return {
                restrict: "E",

                scope: {
                    route: "@"
                },

                template: t
            };
        }
    )
    ;
} // directive

());