(function () {
    'use strict';
    angular.module('app.dir').directive('btnLink',


        function () {

            var t = '<a ui-sref="{{state}}" class="btn btn-info btn-lg btn-block btn-raised"\
            ><i class="material-icons">{{iconText}}</i> {{text}}</a>';
            return {
                restrict: "E",

                scope: {
                    text: "@",
                    iconText: "@",
                    state: "@"
                },


                template: t
            };
        }
    )
    ;
} // directive

());