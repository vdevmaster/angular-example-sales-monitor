(function (){
    'use strict';
    angular.module('app').directive('formLabel',[

            function() {

                var t = '<label class="col-md-2 control-label" for="{{formName}}">{{formLabel}}</label>';

                return {
                    restrict:"E",

                    link: function (scope,el,attr) {

                        scope.formLabel = attr.label;
                        scope.formName = attr.name;
                    },

                    template:t

                };
            }]);
} // directive

());