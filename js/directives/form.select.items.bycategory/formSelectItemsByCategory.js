
(function () {
    'use strict';
    angular.module('app.dir').directive('formSelectItemsByCategory',

        function () {

            return {
                restrict: "E",

                scope: {
                    label: "@",
                    name: "@",
                    optionLabel:"@",
                    items:"=",
                    resultItem: "=",
                    req:"@",
category:"="
                },
                link: function (scope, el, attr) {

                    scope.req = false;
                    if (attr.req) {
                        scope.req = attr.req;
                    }
                },
                templateUrl: "js/directives/form.select.items.bycategory/form_select_items_by_category.html"

            };
        }
    )
    ;
} // directive

());