(function () {
    'use strict';
    angular.module('app.dir').directive('formBtnSubmit',


        function () {


            var t = '<button type="submit" class="btn btn-primary btn-lg btn-block btn-raised"\
            ng-disabled="disabledCondition"><i class="material-icons">{{iconText}}</i> {{text}}</button>';
            return {
                restrict: "E",

                scope: {
                    text: "@",
                    disabledCondition: "=",
                    iconText: "@"
                },
                link: function (scope, el, attr) {
                    scope.iconText = "done";
                    if (attr.iconText) {
                        scope.iconText = attr.iconText;
                    }

                }
                ,
                template: t
            };
        }
    )
    ;
} // directive

());