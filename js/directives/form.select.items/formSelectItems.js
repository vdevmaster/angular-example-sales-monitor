(function () {
    'use strict';
    angular.module('app.dir').directive('formSelectItems',

        function () {

            return {
                restrict: "E",

                scope: {
                    label: "@",
                    name: "@",
                    optionLabel:"@",
                    items:"=",
                    resultItem: "=",
                    req:"@"

                },
                link: function (scope, el, attr) {

                    scope.req = false;
                    if (attr.req) {
                        scope.req = attr.req;
                    }
                },
                templateUrl: "js/directives/form.select.items/form_select_items.html"

            };
        }
    )
    ;
} // directive

());