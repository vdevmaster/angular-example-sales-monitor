(function () {
    'use strict';
    angular.module('app.dir').directive('progressAction',

        function () {

            return {
                restrict: "E",

                scope: {
                    message: "@",
                    show: "="
                },
                link: function (scope, el, attr) {

                },
                templateUrl: "js/directives/progress.action/progress_action.html"

            };
        }
    )
    ;
} // directive

());