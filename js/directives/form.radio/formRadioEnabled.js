(function () {
    'use strict';
    angular.module('app.dir').directive('formRadioEnabled',

        function () {

            return {
                restrict: "E",

                scope: {
                    label: "@",
                    model: "=",//need for var binding
                    name: "@"
                },
                link: function (scope, el, attr) {
                },
                templateUrl: "js/directives/form.radio/form_radio_enabled.html"

            };
        }
    )
    ;
} // directive

());