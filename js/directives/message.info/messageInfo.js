(function () {
    'use strict';
    angular.module('app.dir').directive('messageInfo',
["MessageService",
        function (MessageService) {

            return {
                restrict: "E",

                scope: {
                },


                controller: ["$scope",function ($scope) {

                    $scope.clearInfo = function () {
                        MessageService.setInfo("");
                        $scope.info= "";
                    };

                    $scope.clearSuccess = function () {
                        MessageService.setSuccess("");
                        $scope.success = "";
                    };

                    $scope.clearError = function () {
                        MessageService.setError("");
                        $scope.error = "";
                    };

                }],
                link: function (scope, el, attr) {

                    scope.info = MessageService.getInfo();
                    scope.error = MessageService.getError();
                    scope.success = MessageService.getSuccess();

                },

                templateUrl: "js/directives/message.info/message_info.html"
            };
        }]
    )
    ;
} // directive

());