(function () {
    'use strict';
    angular.module('app.dir').directive('inputText',

        function () {

            return {
                restrict: "E",

                scope: {
                    label: "@",
                    model: "=",//need for var binding
                    name: "@",
                    req: "@",
                    type:"@",

                },

                link: function (scope, el, attr) {

                    scope.req = false;
                    if (attr.req) {
                        scope.req = attr.req;
                    }

                    scope.type = "text";
                    if(attr.type) {
                        scope.type = attr.type;
                    }


                },
                templateUrl: "js/directives/form.input.text/input_text.html"
            };
        }
    )
    ;
} // directive

());