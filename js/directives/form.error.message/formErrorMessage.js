(function () {
    'use strict';
    angular.module('app.dir').directive('formErrorMessage',

        function () {

            return {
                restrict: "E",

                scope: {
                    errorMessage: "@",
                    showErrorExpr: "="
                },

                link: function (scope, el, attr) {

                    scope.errorMessage = "Ошибка";
                    if (attr.errorMessage) {
                        scope.errorMessage = attr.errorMessage;
                    }

                },
                templateUrl: "js/directives/form.error.message/form_error_message.html"
            };
        }
    )
    ;
} // directive

());