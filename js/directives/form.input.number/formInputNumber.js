
(function () {
    'use strict';
    angular.module('app.dir').directive('formInputNumber',

        function () {

            return {
                restrict: "E",

                scope: {
                    label: "@",
                    model: "=",//need for var binding
                    name: "@",
                    req: "@",
                    min: "@",
                    max:"@"

                },

                link: function (scope, el, attr) {



                    scope.req = false;
                    if (attr.req) {
                        scope.req = attr.req;
                    }

                },
                templateUrl: "js/directives/form.input.number/input_number.html"
                //   template:t
            };
        }
    )
    ;
} // directive

());