
(function () {
    'use strict';

    angular.module('app').factory('NavigationService',
        ['$rootScope',
            '$state', '$log',
            function ($rootScope, $state, $log) {


                $rootScope.navWorkpoint = false;
                $rootScope.navUser = false;
                $rootScope.navCategory = false;
                $rootScope.navItem = false;
                $rootScope.navOder = false;
                $rootScope.navRequest = false;
                $rootScope.navReg = false;
                $rootScope.navLogin = false;

                var disable = function() {
                    $rootScope.navWorkpoint = false;
                    $rootScope.navUser = false;
                    $rootScope.navCategory = false;
                    $rootScope.navItem = false;
                    $rootScope.navOrder = false;
                    $rootScope.navRequest = false;
                    $rootScope.navReg = false;
                    $rootScope.navLogin = false;
                };

                var service = {

                 activeWorkpoint :function() {

                     disable();
                     $rootScope.navWorkpoint = true;
                },
                    activeUser :function() {

                        disable();
                        $rootScope.navUser= true;
                    },
                    activeCategory :function() {

                        disable();
                        $rootScope.navCategory = true;
                    },
                    activeItem :function() {

                        disable();
                        $rootScope.navItem = true;
                    },
                    activeOrder :function() {

                        disable();
                        $rootScope.navOrder = true;
                    },
                    activeRequest :function() {

                        disable();
                        $rootScope.navRequest = true;
                    },
                    activeReg :function() {

                        disable();
                        $rootScope.navReg = true;
                    },
                    activeLogin :function() {

                        disable();
                        $rootScope.navLogin = true;
                    },

                };

                return service;



}]); //factory
}());