
(function () {
    'use strict';

    angular.module('app').factory('OrderService',
        ['$rootScope', '$firebaseAuth', '$firebaseArray',
            '$state', 'FIREBASE_URL', '$log', 'MessageService', 'WorkpointService','$firebaseObject',

            function ($rootScope, $firebaseAuth, $firebaseArray,
                      $state, FIREBASE_URL, $log, MessageService, WorkpointService,$firebaseObject) {

                var ordersRef = new Firebase(FIREBASE_URL + 'users/' +
                    $rootScope.currentUser.owner + '/orders');

                var ordersListInfo = $firebaseArray(ordersRef);


                var service = {

                    get: function (id) {
                        return ordersListInfo.$getRecord(id);
                    },

                    getAll: function () {
                        return ordersListInfo;
                    },

                    remove: function (order) {
                        $rootScope.showLoading = true;
                        ordersListInfo.$remove(order).then(function(ref){
                            MessageService.setSuccess("Заказ удален!");
                            $rootScope.showLoading = false;
                        },function(error) {
                            MessageService.setError(error);
                            $rootScope.showLoading = false;
                        });
                    },


                    getOrderItems: function (order) {
                        $log.debug("In get orders item")
                        $log.debug(order);
                        var ordersItemsRef = new Firebase(FIREBASE_URL + 'users/' +
                            $rootScope.currentUser.owner + '/orders/' + order.$id + "/ordersItems");
                        var ordersItemsListInfo = $firebaseArray(ordersItemsRef);
                        return ordersItemsListInfo;

                    },

                    createOderItem: function (order, orderItem) {

                        var obj = {};
                        obj.title = orderItem.item.title;
                        obj.categoryId = orderItem.category.$id;
                        obj.itemId = orderItem.item.$id;
                        obj.count = orderItem.count;
                        obj.price = service.calculatePriceForOderItem(orderItem);
                        service.getOrderItems(order).$add(obj);
                        MessageService.setSuccess("Позиция для заказа добавлена!");
                        $state.go('order.create', {id: order.$id});
                    },

                    calculatePriceForOderItem: function(orderItem) {
                        var result = 0;

                            result = parseInt(orderItem.count) * parseInt(orderItem.item.price);

                        return result;
                    },

                    updateOrderItem: function (order, orderItem) {

                        order.title = orderItem.item.title;
                        orderItem.categoryId = orderItem.category.$id;
                        orderItem.itemId = orderItem.item.$id;
                        orderItem.price = parseInt(orderItem.count) * parseInt(orderItem.item.price);
                        orderItem.category = null;
                        orderItem.item = null;

                        service.getOrderItems(order).$save(orderItem);

                        MessageService.setSuccess("Позиция для заказа добавлена!");
                        $state.go('order.create', {orderId: order.$d});

                    },
                    removeOrderItem: function (order, orderItem) {
                        service.getOrderItems(order).$remove(orderItem);
                        MessageService.setSuccess("Удаленна позиция из заказа!");
                        //$state.go('order.create', {orderId: order.$id});
                    },

                    create: function () {
                        $rootScope.showLoading = true;

                        var workpoint = WorkpointService.get($rootScope.currentUser.workpointId);

                            var order = {};
                            //published
                            order.enabled = false;
                            order.created = Firebase.ServerValue.TIMESTAMP;
                            order.updated = Firebase.ServerValue.TIMESTAMP;
                            order.comment = "";
                            order.userCreated = $rootScope.currentUser.$id;
                            order.customDate = Firebase.ServerValue.TIMESTAMP;
                            order.discountComment = "";
                            order.customCost = 0;
                            order.discount = 0;
                            order.checkNumber = "";
                            order.totalCost = 0;
                            order.workpoint = workpoint.$id;
                        order.title = "";
                            ordersListInfo.$add(order).then(function (ref) {
                                $log.debug("Создана новая продажа!");
                                var id = ref.key();
                                console.log("added record with id " + id);
                                $log.debug(ordersListInfo.$indexFor(id));
                                $rootScope.showLoading = false;
                                MessageService.setSuccess("Новый заказ!");
                                $state.go('order.create', {id: ref.key()});
                            });



                    },

                    temp: function (order,orderItems,workpoint) {
                        $rootScope.showLoading = true;
                        order = service.fill(order,orderItems,workpoint);
                        ordersListInfo.$save(order);
                        $rootScope.showLoading = false;
                        MessageService.setSuccess("Черновик сохранен ");
                        $state.go("order.list");

                    },

                    fill:function(order,orderItems,workpoint) {
                        order.updated = Firebase.ServerValue.TIMESTAMP;
                        order.customCost = parseInt(order.customCost);
                        order.comment = order.comment || "";
                        order.checkNumber = order.checkNumber || "";
                        order.customCost = parseInt(order.customCost) || 0;
                        order.discount = parseInt(order.discount) || 0;
                        order.discountComment = order.discountComment  || "";
                        order.title = service.setTitle(orderItems);
                        var totalCost = service.calculateTotalCost(orderItems, order.customCost,order.discount, workpoint.percentRate);
                        order.totalCost = totalCost;
                        return order;
                    }

                    , finish: function (order,orderItems,workpoint) {
                        $log.debug(order);
                        $rootScope.showLoading = true;
                        order = service.fill(order,orderItems,workpoint);
                        order.enabled = true;
                        ordersListInfo.$save(order);
                        $rootScope.showLoading = false;
                        MessageService.setSuccess("Опубликовано!");
                        $state.go("order.list");
                    },

                    /**
                     *Вычисление стоимости заказа
                     * @param ordersItems collection objects not null -  orderItems for order
                     * @param customCost value
                     * @param discount value
                     * @param workpointPercentRate value from workpoint.percentRate
                     * @returns {*} Calculation result
                     */
                    calculateTotalCost: function (ordersItems, customCost,discount, workpointPercentRate) {

                        customCost = parseInt(customCost);
                        workpointPercentRate = parseInt(workpointPercentRate);
                        discount = parseInt(discount);
                        var orderItemResultCost = 0;

                        if (ordersItems.length > 0) {
                            orderItemResultCost = _.reduce(ordersItems, function (summ, orderItem) {

                                return summ + parseInt(orderItem.price);
                            }, orderItemResultCost);
                        }

                        var totalCost = 0;

                        if (customCost < 0) {
                            totalCost -= customCost;
                        } else {
                            totalCost += customCost;
                        }

                        totalCost += orderItemResultCost;

                        if (workpointPercentRate > 0) {
                            workpointPercentRate = parseFloat(workpointPercentRate / 100);
                            totalCost = totalCost + (totalCost * workpointPercentRate);
                        } else if (workpointPercentRate < 0) {
                            workpointPercentRate = parseFloat(workpointPercentRate / 100);
                            totalCost = totalCost - (totalCost * workpointPercentRate);
                        }


                        if(discount > 0) {
                            totalCost = totalCost - discount;
                        }

                        return totalCost;
                    }

                    /**
                     * Для отображения позиции заказа
                     * @param orderItems
                     * @returns {string}
                     */
                    ,setTitle: function(orderItems) {
                        var title = "";
                        if(orderItems && orderItems.length >0) {
                            _.each(orderItems,function(item) {
                                title += item.title + " x " + item.count+"";
                            })
                        }
                        return title;
                    }
                    ,calculateOrderCost:function(orders) {

                        var summ = 0;

                        var total = _.reduce(orders,function(s,order) {
                            return s + parseInt(order.totalCost);
                        },summ);

                        return total;
                    }

                };

                return service;

            }]); //factory
}());