
(function () {
    'use strict';

    angular.module('app').factory('MessageService',
        ['$log',

            function ($log) {

                var infoMessage= "",errorMessage= "",successMessage = "";

                var service = {

                    getInfo: function () {
                        return infoMessage;
                    },

                    setInfo:function(message) {
                        infoMessage = message;
                    }
                    ,

                    getError: function () {
                        return errorMessage;
                    },

                    setError:function(message) {
                        errorMessage = message;
                    }
                    ,

                    getSuccess: function () {
                        return successMessage;
                    },


                    setSuccess:function(message) {
                        successMessage = message;
                    }

                    ,
                    clearAll:function() {
                        successMessage ="";
                        errorMessage = "";
                        infoMessage = "";
                    }
                };

                return service;

            }]); //factory
}());
