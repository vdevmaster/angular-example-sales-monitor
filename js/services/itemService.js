
(function () {
    'use strict';

    angular.module('app').factory('ItemService',
        ['$rootScope', '$firebaseAuth', '$firebaseArray',
            '$state', 'FIREBASE_URL','$log','MessageService',

            function ($rootScope, $firebaseAuth, $firebaseArray,
                      $state, FIREBASE_URL,$log,MessageService) {

                var itemsRef = new Firebase(FIREBASE_URL + 'users/' +
                    $rootScope.currentUser.owner + '/items');

                var itemsListInfo = $firebaseArray(itemsRef);

                var service = {

                    get: function (id) {
                        return itemsListInfo.$getRecord(id);
                    },
                    getAll: function () {
                        return itemsListInfo;
                    },
                    create: function (item) {
                        $rootScope.showLoading = true;
                        item.enabled = true;
                        item.created = Firebase.ServerValue.TIMESTAMP;
                        item.updated = Firebase.ServerValue.TIMESTAMP;
                        item.categoryId = item.category.$id;
                        item.category = null;
                        item.article = item.article || "";
                        item.decription = item.decription || "";

                        itemsListInfo.$add(item);
                            $rootScope.showLoading = false;
                        MessageService.setSuccess("Товар (услуга) "+item.title+" создана!");
                           $state.go('item.list');
                    },

                    update: function(item) {
                        $rootScope.showLoading = true;
                        item.categoryId = item.category.$id;
                        item.category = null;
                        item.updated = Firebase.ServerValue.TIMESTAMP;
                        item.article = item.article || "";
                        item.decription = item.decription || "";
                        itemsListInfo.$save(item);
                        $rootScope.showLoading = false;
                        MessageService.setSuccess("Товар (услуга) "+item.title+" обновлена!");
                        $state.go("item.list");
                    }
                };

                return service;

            }]); //factory
}());