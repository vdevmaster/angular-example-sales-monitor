(function () {
    'use strict';

    angular.module('app').factory('Authentication',
        ['$rootScope', '$firebaseAuth', '$firebaseObject',
            '$state', 'FIREBASE_URL', '$log',
            function ($rootScope, $firebaseAuth, $firebaseObject,
                      $state, FIREBASE_URL, $log) {

                var ref = new Firebase(FIREBASE_URL);
                var auth = $firebaseAuth(ref);

                auth.$onAuth(function (authUser) {
                    if (authUser) {
                        var userRef = new Firebase(FIREBASE_URL + 'users/' + authUser.uid);

                        var userObj = $firebaseObject(userRef);

                        userObj.$loaded().then(function (ref) {


                            if (userObj.isOwner) {
                                checkUser(userObj);
                            } else {
                                var uR = new Firebase(FIREBASE_URL + 'users/' + userObj.ownerRef + "/users/" + authUser.uid);
                                var workerRef = $firebaseObject(uR);
                                workerRef.$loaded().then(function (baseRef) {
                                    checkUser(workerRef);
                                }).catch(function (error) {
                                    $rootScope.message = error;
                                });

                            }

                        }).catch(function (error) {
                            $log.error(error);
                            hideLoading();
                            fail();
                        });

                        //$rootScope.currentUser = userObj;
                    } else {
                        //$rootScope.currentUser = '';
                    }
                });

                var checkUser = function (user) {
                    $rootScope.hasWorkerFeatures = true;
                    $rootScope.currentUser = user;
                    hideLoading();
                    clearMessage();

                    if (user.isOwner && user.enabled) {
                        $rootScope.hasOwnerFeatures = true;
                        $state.go('order.list');
                    } else if (!user.isOwner && user.enabled == true) {
                        $state.go('order.list');
                    } else if (user.enabled == false) {
                        $rootScope.message = "Доступ закрыт - пользователь заблокирован";
                        fail();
                    } else {

                        $rootScope.message = "";
                        fail();
                    }
                };

                var fail = function () {
                    auth.$unauth();
                    $rootScope.currentUser = '';
                    $rootScope.hasWorkerFeatures = false;
                    $state.go('login');
                };

                var showLoading = function () {
                    $rootScope.showLoading = true;
                    //todo move to global event state
                };

                var hideLoading = function () {
                    $rootScope.showLoading = false;
                };


                var clearMessage = function () {
                    $rootScope.message = "";
                };

                var myObject = {
                    login: function (user) {
                        showLoading();
                        auth.$authWithPassword({
                            email: user.email,
                            password: user.password
                        }).then(function (regUser) {
                            $log.debug(regUser);

                        }).catch(function (error) {
                            hideLoading();
                            $log.error(error);
                            $rootScope.message = error.message;
                        });
                    }, //login


                    logout: function () {
                        $rootScope.message = "";
                        $rootScope.hasWorkerFeatures = false;
                        $rootScope.hasOwnerFeatures = false;
                        $state.go('login');
                        return auth.$unauth();
                    }, //logout

                    requireAuth: function () {
                        return auth.$requireAuth();
                    }, //require Authentication


                    register: function (user) {
                        auth.$createUser({
                            email: user.email,
                            password: user.password
                        }).then(function (regUser) {
                            var regRef = new Firebase(FIREBASE_URL + 'users')
                                .child(regUser.uid).set({
                                    date: Firebase.ServerValue.TIMESTAMP,
                                    updated: Firebase.ServerValue.TIMESTAMP,
                                    regUser: regUser.uid,
                                    firstname: user.firstname,
                                    lastname: user.lastname,
                                    middlename: user.middlename,
                                    email: user.email,
                                    company: user.company,
                                    owner: regUser.uid,
                                    enabled: true,
                                    users: "",
                                    requests: "",
                                    categories: "",
                                    items: "",
                                    responses: "",
                                    sales: "",
                                    isOwner: true
                                }); //user info


                            myObject.login(user);

                        }).catch(function (error) {
                            $rootScope.message = error.message;
                        }); // //createUser
                    } // register
                };

                return myObject;
            }]); //factory
}());
