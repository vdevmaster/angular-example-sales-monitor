
(function () {
    'use strict';

    angular.module('app').factory('UserService',
        ['$rootScope', '$firebaseAuth', '$firebaseArray',
            '$state', 'FIREBASE_URL', '$log', 'MessageService',"$firebaseObject",

            function ($rootScope, $firebaseAuth, $firebaseArray,
                      $state, FIREBASE_URL, $log, MessageService,$firebaseObject) {


                var ref = new Firebase(FIREBASE_URL);
                var auth = $firebaseAuth(ref);

                var usersRef = new Firebase(FIREBASE_URL + 'users/' +
                    $rootScope.currentUser.$id + '/users');

                var usersListInfo = $firebaseArray(usersRef);

                var service = {

                    //used in user.edit
                    get: function (id) {
                        return usersListInfo.$getRecord(id);
                    },

                    //used in user.list
                    getAll: function () {
                        return usersListInfo;
                    },

                    //used on user.create
                    create: function (user) {
                        $rootScope.showLoading = true;
                        auth.$createUser({
                            email: user.email,
                            password: user.password
                        }).then(function (regUser) {
                            var regRef = new Firebase(FIREBASE_URL + 'users/' + $rootScope.currentUser.$id + "/users");
                            regRef.child(regUser.uid).set({
                                created: Firebase.ServerValue.TIMESTAMP,
                                updated: Firebase.ServerValue.TIMESTAMP,
                                regUser: regUser.uid,
                                firstname: user.firstname || "",
                                lastname: user.lastname || "",
                                middlename: user.middlename || "",
                                email: user.email,
                                company: $rootScope.currentUser.company,
                                owner: $rootScope.currentUser.$id,
                                enabled: user.enabled,
                                workpointId: user.workpoint.$id,
                                isOwner:false
                            }); //user info
                            var userRef = new Firebase(FIREBASE_URL + 'users/' + regUser.uid);

                            userRef.set({
                                isOwner: false,
                                ownerRef: $rootScope.currentUser.$id
                            });

                            $rootScope.showLoading = false;
                            MessageService.setSuccess("Пользователь " + user.email + "создан!");

                            $state.go('user.list');
                        }).catch(function (error) {
                            $rootScope.showLoading = false;
                            $rootScope.message = error.message;
                            MessageService.setError(error);
                        }); //
                    },

                    update: function (user) {
                        user.updated = Firebase.ServerValue.TIMESTAMP;

                        user.workpointId = user.workpoint.$id;
                        user.workpoint = null;
                        usersListInfo.$save(user);
                        $rootScope.showLoading = false;
                        MessageService.setSuccess("Пользователь " + user.email + "обновлен!");
                        $state.go("user.list");
                    }

                    , updateProfile: function (user) {


                        var update = {};
                        update.firstname = user.firstname || "";
                        update.lastname = user.lastname || "";
                        update.middlename = user.middlename|| "";
                        update.company = user.company ||
                        "";

                        update.updated = Firebase.ServerValue.TIMESTAMP;

                        if (user.workpoint) {
                            update.workpointId  = user.workpoint.$id;
                            user.workpoint = null;
                        }

                        var userRef = new Firebase(FIREBASE_URL + 'users/' + user.$id);
                        userRef.update(update,function (error) {
                            if(error) {
                                $log.error("Error:", error);
                                $rootScope.showLoading = false;
                                MessageService.setError(""+error);
                                $state.go("user.list");
                            }else{
                                $log.debug("Updated user is equals: "+(ref.key() === user.$id));
                                $rootScope.currentUser = user;
                                $rootScope.showLoading = false;
                                MessageService.setSuccess("Пользователь " + user.email + " обновлен!");
                                $state.go("user.list");
                            }
                        });
                    }


                };

                return service;

            }]); //factory
}());