
(function () {
    'use strict';

    angular.module('app').factory('CategoryService',
        ['$rootScope', '$firebaseAuth', '$firebaseArray',
            '$state', 'FIREBASE_URL','$log','MessageService',

            function ($rootScope, $firebaseAuth, $firebaseArray,
                      $state, FIREBASE_URL,$log,MessageService) {

                var categoryRef = new Firebase(FIREBASE_URL + 'users/' +
                    $rootScope.currentUser.owner + '/categories');

                var categoriesListInfo = $firebaseArray(categoryRef);

                var service = {

                    get: function (id) {
                        return categoriesListInfo.$getRecord(id);
                    },
                    getAll: function () {
                        return categoriesListInfo;
                    },
                    create: function (category) {
                        $rootScope.showLoading = true;
                        category.enabled = true;
                        category.created = Firebase.ServerValue.TIMESTAMP,
                        category.updated = Firebase.ServerValue.TIMESTAMP;
                        categoriesListInfo.$add(category);
                            $rootScope.showLoading = false;
                        MessageService.setSuccess("Создана новая категория: "+category.title+" !");
                           $state.go('category.list');
                    },

                    update: function(category) {
                        $rootScope.showLoading = true;
                        category.updated = Firebase.ServerValue.TIMESTAMP,
                        categoriesListInfo.$save(category);
                        $rootScope.showLoading = false;
                        MessageService.setSuccess("Категория: "+category.title+" обновлена!");
                        $state.go("category.list");
                    }
                };

                return service;

            }]); //factory
}());