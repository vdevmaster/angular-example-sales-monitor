(function () {
    'use strict';

    angular.module('app').factory('RequestService',
        ['$rootScope', '$firebaseAuth', '$firebaseArray',
            '$state', 'FIREBASE_URL','$log','MessageService',

            function ($rootScope, $firebaseAuth, $firebaseArray,
                      $state, FIREBASE_URL,$log,MessageService) {

                var requestsRef = new Firebase(FIREBASE_URL + 'users/' +
                    $rootScope.currentUser.owner + '/requests');

                var requestListInfo = $firebaseArray(requestsRef);

                var service = {

                    get: function (id) {
                        return requestListInfo.$getRecord(id);
                    },
                    getAll: function () {
                        return requestListInfo;
                    },
                    create: function (request,workpoint) {
                        $rootScope.showLoading = true;
                        var record = {};
                        record.title = request.title;
                        record.body = request.body;
                        record.created = Firebase.ServerValue.TIMESTAMP;
                        record.userCreated = $rootScope.currentUser.$id;
                        record.workpointId = workpoint.$id;
                        record.enabled = true;
                        requestListInfo.$add(record);
                        $rootScope.showLoading = false;
                        MessageService.setSuccess("Заявка "+request.title+" создана!");
                        $state.go('request.list');
                    }
                };

                return service;

            }]);
}());
