
(function () {
    'use strict';

    angular.module('app').factory('WorkpointService',
        ['$rootScope', '$firebaseAuth', '$firebaseArray',
            '$state', 'FIREBASE_URL','$log','MessageService',

            function ($rootScope, $firebaseAuth, $firebaseArray,
                      $state, FIREBASE_URL,$log,MessageService) {

                var workpointsRef = new Firebase(FIREBASE_URL + 'users/' +
                    $rootScope.currentUser.owner + '/workpoints');

                var workpointsListInfo = $firebaseArray(workpointsRef);

                var service = {


                    getName:function(workkpointId) {
                        return service.get(workkpointId).title;
                    },

                    get: function (id) {
                        return workpointsListInfo.$getRecord(id);
                    },
                    getAll: function () {
                        return workpointsListInfo;
                    },
                    create: function (workpoint) {
                        $rootScope.showLoading = true;
                        workpoint.enabled = true;
                        workpointsListInfo.$add(workpoint);
                            $rootScope.showLoading = false;
                        MessageService.setSuccess("Точка продаж "+workpoint.title+" создана!");
                           $state.go('workpoint.list');
                    },

                    update: function(workpoint) {
                        $rootScope.showLoading = true;
                        workpoint.updated = Firebase.ServerValue.TIMESTAMP;
                        workpointsListInfo.$save(workpoint);
                        $rootScope.showLoading = false;
                        MessageService.setSuccess("Точка продаж: "+workpoint.title+" обновлена!");
                        $state.go("workpoint.list");
                    }
                };

                return service;

            }]);
}());